package com.picatsu.akatsuki.config;


import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;


 import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
 public class SwaggerConfig {
    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI()
                .info(new Info()
                        .title("Swagger doc for the api of akatsuki nuit de l info project")
                        .version("0.0.1")
                        .description("open source project, Founders : Achraf L.")
                        .termsOfService("http://swagger.io/terms/")
                        .license(new License().name("Apache 2.0").url("http://springdoc.org")));
    }

    @Bean("customFixedThreadPool")
    public ExecutorService customFixedThreadPool() {
        return Executors.newFixedThreadPool(2,new CustomizableThreadFactory("customFixedThreadPool"));
    }
}
