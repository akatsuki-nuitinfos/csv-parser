package com.picatsu.akatsuki.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class VilleData {
    @Id
    private String id;

    private String adresse;
    private String description;
    private String nom;
    private String prenom;
    private String email;
    private String tel;
    private String photo;

}
