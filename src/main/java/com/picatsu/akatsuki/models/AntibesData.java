package com.picatsu.akatsuki.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "antibes")
public class AntibesData {


    private String nom;
    private String siret;
    private String indicateur;
    private String nom_indicateur;
    private Map<String, Object> years = new HashMap<>();

}
