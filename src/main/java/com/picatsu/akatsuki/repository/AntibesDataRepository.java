package com.picatsu.akatsuki.repository;

import com.picatsu.akatsuki.models.AntibesData;
import com.picatsu.akatsuki.models.VilleData;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AntibesDataRepository extends MongoRepository<AntibesData, String> {

}
