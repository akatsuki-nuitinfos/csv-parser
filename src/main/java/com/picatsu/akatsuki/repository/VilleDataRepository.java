package com.picatsu.akatsuki.repository;

import com.picatsu.akatsuki.models.VilleData;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface VilleDataRepository extends MongoRepository<VilleData, String> {
    long deleteAllById(String id);
}
