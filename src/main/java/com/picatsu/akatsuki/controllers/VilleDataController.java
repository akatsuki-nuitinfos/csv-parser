package com.picatsu.akatsuki.controllers;


import com.picatsu.akatsuki.models.VilleData;
import com.picatsu.akatsuki.repository.VilleDataRepository;

import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.io.IOException;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController("VilleDataController")
@RequestMapping("/api/v1/villes")
@Slf4j
public class VilleDataController {
    @Autowired
    private VilleDataRepository villeDataRepository;


    @GetMapping(value = "/")
    @Operation(summary = "Get All events of a city ")
    public List<VilleData> getAllEvents(){

        log.info("Getting events details " );
        return villeDataRepository.findAll() ;
    }


    @PostMapping(value = "/add")
    @Operation(summary = "Add events ")
    public VilleData postEvents(@RequestBody VilleData v ) {

        log.info("Add events into db ");
        return villeDataRepository.save(v);
    }


    @DeleteMapping(value= "/{id}")
    @Operation(summary = "delete events from db")
    public ResponseEntity<?> deleteEvents(@PathVariable(value= "id") String id) {
        log.info("deleting event :  " + id);

        long val =  villeDataRepository.deleteAllById(id);

        if ( val == 1) {
            return new ResponseEntity<>("Deleted successfully ", HttpStatus.OK);
        }
        if( val == 0 ) {
            return new ResponseEntity<>("Cannot find Symbol : " + id, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>("Obscure error ", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
