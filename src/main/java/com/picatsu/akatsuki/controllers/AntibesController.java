package com.picatsu.akatsuki.controllers;

import com.picatsu.akatsuki.models.AntibesData;
import com.picatsu.akatsuki.repository.AntibesDataRepository;
import com.picatsu.akatsuki.service.AntibesDataService;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController("AntibesController")
@RequestMapping("/api/v1/antibes")

public class AntibesController {

	@Autowired
	private AntibesDataService antibesDataService;

	@Autowired
	private AntibesDataRepository antibesDataRepository;

	@GetMapping("/raw")
	public List<AntibesData> Parse() throws IOException {
		return antibesDataService.getAntibesData();

	}

	@GetMapping("/")
	public List<AntibesData> All() throws IOException {
		return antibesDataRepository.findAll();

	}


}
