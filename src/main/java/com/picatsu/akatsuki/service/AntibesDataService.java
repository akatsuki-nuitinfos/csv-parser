package com.picatsu.akatsuki.service;


import com.picatsu.akatsuki.models.AntibesData;
import com.picatsu.akatsuki.repository.AntibesDataRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
@Slf4j
public class AntibesDataService {

    @Autowired
    private AntibesDataRepository antibesDataRepository;

    public List<AntibesData> getAntibesData() throws IOException {

            List<AntibesData> antibesDataList = new ArrayList<>();
            File resource = ResourceUtils.getFile("classpath:antibes-eau.csv");

            System.out.println(resource.getAbsolutePath());
            BufferedReader reader = null;
            try {
                FileReader fichier = new FileReader(resource.getAbsolutePath());
                reader = new BufferedReader(fichier);
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
            String myLine = "";
            try{
                myLine  = reader.readLine();
                System.out.println(" ligne 1 : " + myLine);

            }catch( Exception e){
                System.out.println( " ERROR READLINE FIRST LINE ");
            }

            while ((myLine = reader.readLine()) != null ) {

                antibesDataList.add( this.parseLine(myLine));

            }

            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            for(AntibesData e: antibesDataList) {
                antibesDataRepository.save(e);
            }

            return antibesDataList;
    }


    private AntibesData parseLine(String ligne) {
        AntibesData a = new AntibesData();
        String[] line = ligne.split(";");
        a.setNom(line[0]);
        a.setSiret(line[1]);
        a.setIndicateur(line[2]);
        a.setNom_indicateur(line[3]);
        Map<String, Object> map = new HashMap<>();

        map.put("2016", line[4]);
        map.put("2017", line[5]);
        map.put("2018", line[6]);
        map.put("2019", line[7]);
        a.setYears(map);

        return a;
    }


}
