#! /bin/bash

./gradlew clean build -x test

JAR_FILE=$(ls build/libs/ | grep "^akatsuki")
echo $JAR_FILE

docker build . --build-arg jar=build/libs/$JAR_FILE -t ezzefiohez/nuitinfos-api-csv-ville
docker push ezzefiohez/nuitinfos-api-csv-ville


#curl  -X POST link
