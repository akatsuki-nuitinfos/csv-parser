FROM openjdk:12

ARG jar

WORKDIR /workspace/app

COPY $jar app.jar

ENTRYPOINT ["java", "-jar",  "app.jar"]

