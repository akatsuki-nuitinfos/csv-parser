# Akatsuki nuit de l'info 

# Readme api service 

Ce repertoire git contient le source de l'Api réaliser par l'équipe Akatsuki dans la cadre du défi nuit de l'info 2020. 

API des défis : 
    https://www.nuitdelinfo.com/inscription/defis/265 - MyCity
    https://www.nuitdelinfo.com/inscription/defis/269 - Données vertes


* [Lien Documentation Swagger](http://146.59.248.65:8001/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config)


## Installation

### Clone repo

```bash
# clone the repo
$ git clone <repoLink>

# go into app's directory
$ cd project name

# install app's dependencies
$ ./gradlew bootRun or ($ npm install && npm start  for web client)
```

## Usage Intellij

```bash
# step 1
$ Open One of the project in intellij

# step 2
$ file > Project structure > Modules > + > import module

# step 3
$ now you can run the 3 api inside one intellij instance

```

## Prerequisites

- Intellij / vscode 
- Java (jdk 11 ) + Gradle 
- Postman + ModHeader + Prettier plugin

